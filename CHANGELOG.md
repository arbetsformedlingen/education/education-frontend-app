# 1.2.4
* Updated Angular to version 17.3.12
* Updated various packages including Angular Material, CDK, and other dependencies
* Fixed security issue related to 'Improper Input Validation'
* Added support for Angular 17 compatibility
* Added debug flag as query string
* Option to hide/show Demo-GUI version based on debug setting
* Added municipality data to education events
* Implemented debug flag functionality
* Enabled multiple values input for education form and education type
* Updated CI configuration for Jobtech deployment
* Enhanced support for string arrays in education form and type selections

# 1.0.0
* Initial release
