FROM node:18.20.2-alpine3.19 AS build
WORKDIR /usr/src/app
COPY . .
RUN npm install &&\
    npm run build

FROM nginxinc/nginx-unprivileged:1.27-alpine3.19-slim
RUN rm /etc/nginx/conf.d/default.conf
COPY --from=build /usr/src/app/dist/education-frontend-app /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
