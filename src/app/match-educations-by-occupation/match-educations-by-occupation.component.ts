import { Component, OnInit } from '@angular/core';
import {
  UntypedFormControl,
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
  AbstractControl,
  FormControl
} from "@angular/forms";
import { MatchEducationsByOccupationService } from "../services/match-educations-by-occupation.service";
import { MatchEducationsByJobtitleQuery } from "../model/search-queries.model";
import { Searchparameter } from "../model/searchparameter.model";
import { debounceTime, distinctUntilChanged, filter, merge, Observable, of, startWith, switchMap } from "rxjs";
import { map, reduce } from "rxjs/operators";
import { SearchparametersService } from "../services/searchparameters.service";
import { JobtitlesAutocompleteItem } from "../model/matched-educations-by-occupation.model";
import { MatCheckboxChange } from "@angular/material/checkbox";

@Component({
  selector: 'app-match-educations-by-occupation',
  templateUrl: './match-educations-by-occupation.component.html',
  styleUrls: ['./match-educations-by-occupation.component.scss']
})
export class MatchEducationsByOccupationComponent implements OnInit {
  form: UntypedFormGroup = new UntypedFormGroup({
    jobtitleSearchCtrl: new FormControl(''),
    municipalityCodeFilterCtrl: new UntypedFormControl(''),
    distanceSearchCtrl: new UntypedFormControl('')
  })


  jobtitleSearchCtrl: FormControl;
  geoCodeFilterCtrl: FormControl;
  distanceSearchCtrl: UntypedFormControl;

  selectedEducationType: string;
  selectedEducationForm: string;
  educationTypes: Array<Searchparameter>;
  educationForms: Array<Searchparameter>;
  geos: Array<Searchparameter>;
  filteredGeos: Observable<Searchparameter[]>;
  filteredJobtitles: Observable<JobtitlesAutocompleteItem[]>;

  MUNICIPALITY_CODE_LENGTH = 4
  REGION_CODE_LENGTH = 2

  constructor(
    private matchEducationsService: MatchEducationsByOccupationService,
    private searchparametersService: SearchparametersService,
    private formBuilder: UntypedFormBuilder
  ) {
    this.jobtitleSearchCtrl = new FormControl<string>('', [
      CustomValidators.forbiddenObjectValidator,
      Validators.nullValidator
    ]);

    this.selectedEducationType = '';
    this.selectedEducationForm = '';
    this.geoCodeFilterCtrl = new FormControl<string>('', [
      CustomValidators.forbiddenObjectValidator
    ]);
    this.filteredGeos = new Observable<Searchparameter[]>();
    this.distanceSearchCtrl = new UntypedFormControl();
    this.educationTypes = new Array<Searchparameter>();
    this.educationForms = new Array<Searchparameter>();
    this.geos = new Array<Searchparameter>();

    //fetchAutocompleteJobtitles
    this.filteredJobtitles = new Observable<JobtitlesAutocompleteItem[]>()
  }

  ngOnInit(): void {
    this.loadSearchparameters();

    this.filteredGeos = this.geoCodeFilterCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );

    this.filteredJobtitles = this.jobtitleSearchCtrl.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(200),
      filter((name) => !!name),
      switchMap(name => this.matchEducationsService.fetchAutocompleteJobtitles(name))
    );


  }


  displayFnGeo(geo: any): string {
    return geo && geo.value ? geo.value : '';
  }

  displayFnJobtitle(jobtitle: any): string {
    return jobtitle && jobtitle.jobtitle ? jobtitle.jobtitle : '';
  }


  private _filter(value: string): Searchparameter[] {
    if (typeof value === "object") {
      return []
    }

    const filterValue = value.toLowerCase();
    return this.geos.filter(option => option.value?.toLowerCase().startsWith(filterValue));
  }

  private loadSearchparameters() {
    this.searchparametersService.getEducationTypes().subscribe(items => {
      this.educationTypes = items;
    });

    this.searchparametersService.getEducationForms().subscribe(items => {
      this.educationForms = items;
    });

    merge(this.searchparametersService.getMunicipalities(), this.searchparametersService.getRegions()).pipe(reduce((m, r) => m.concat(r))).subscribe(items => {
      this.geos = items;
      const nrOfItems = this.geos.length;
      console.log('Loaded ' + nrOfItems + ' geos');
    });
  }

  matchEducations(): void {

    if (this.jobtitleSearchCtrl.invalid) {
      return;
    }
    const matchEducationsByOccupationQuery: MatchEducationsByJobtitleQuery = {
      jobtitle: '',
      education_type: [],
      education_form: [],
      municipality_code: '',
      region_code: '',
      distance: false,
      limit: 10,
      offset: 0,
      include_metadata: true
    }

    // this.navbarService.displayEducationDetails = false
    // this.navbarService.displayMatchedOccupations = false
    if (this.jobtitleSearchCtrl.value) {
      matchEducationsByOccupationQuery.jobtitle = this.jobtitleSearchCtrl.value.jobtitle;
    }

    if (this.selectedEducationType !== "") {
      matchEducationsByOccupationQuery.education_type = MatchEducationsByOccupationComponent.createListInputParameter(this.selectedEducationType);
    }

    if (this.selectedEducationForm !== "") {
      matchEducationsByOccupationQuery.education_form = MatchEducationsByOccupationComponent.createListInputParameter(this.selectedEducationForm);
    }

    if (this.geoCodeFilterCtrl.value) {
      const geoKey = this.geoCodeFilterCtrl.value.key;

      if (geoKey.length == this.REGION_CODE_LENGTH) {
        matchEducationsByOccupationQuery.region_code = geoKey;
      }
      else if (geoKey.length == this.MUNICIPALITY_CODE_LENGTH) {
        matchEducationsByOccupationQuery.municipality_code = geoKey;
      }
      else {
        console.warn('Unknown geo type selected ' + geoKey)
      }
    }

    if (this.distanceSearchCtrl.value) {
      matchEducationsByOccupationQuery.distance = true;
    }


    console.log('matchEducationsByOccupationQuery:' + JSON.stringify(matchEducationsByOccupationQuery));

    this.matchEducationsService.matchEducationsByJobtitle(matchEducationsByOccupationQuery, false)
  }


  private static createListInputParameter(values: any): Array<string> {
    let inputParameters = [];
    for (let val of values) {
      inputParameters.push(val)
    }
    return inputParameters
  }


  onChangeCheckBox(observable: MatCheckboxChange) {
    // Checkbox value changed. Ignore observable, just call match function...
    this.matchEducations();
  }
}

export class CustomValidators {

  static forbiddenObjectValidator(control: AbstractControl): any {
    // Validate if value is an object (and not e.g. a string value):
    if (control.value) {
      return typeof control.value !== 'object' || control.value === null ? { 'forbiddenObject': true } : null;
    }
    return;
  }
}
