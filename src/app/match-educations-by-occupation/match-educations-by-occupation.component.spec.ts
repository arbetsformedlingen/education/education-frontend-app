import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchEducationsByOccupationComponent } from './match-educations-by-occupation.component';

describe('MatchEducationsByOccupationComponent', () => {
  let component: MatchEducationsByOccupationComponent;
  let fixture: ComponentFixture<MatchEducationsByOccupationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchEducationsByOccupationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchEducationsByOccupationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
