export class OccupationForecasts {
  conceptId: string | undefined;
  ssyk: string | undefined;
  preamble: string | undefined;
  paragraph1: string | undefined;
  paragraph2: string | undefined;
  paragraph3: string | undefined;
  forecasts: Forecast[] = new Array();
}

export class Forecast {
  year: string | undefined;
  demandValue: number | undefined;
  geo: string | undefined;
}

// "ar": "2022",
//   "bristvarde": "4.0",
//   "concept_id": "bZ4J_riZ_zK6",
//   "datatyp": "Bristindex",
//   "geografi": "Riket",
//   "ingress": "Det finns cirka 19 500 anst\u00e4llda anl\u00e4ggningsarbetare i Sverige, varav cirka 3 procent \u00e4r kvinnor och 97 procent \u00e4r m\u00e4n.",
//   "ssyk": "7114",
//   "stycke1": "Arbetsf\u00f6rmedlingen bed\u00f6mer att det kommer vara liten konkurrens om jobben som anl\u00e4ggningsarbetare under det n\u00e4rmaste \u00e5ret. Det inneb\u00e4r att personer som har den utbildning eller erfarenhet som kr\u00e4vs kommer ha stora m\u00f6jligheter till arbete.",
//   "stycke2": "\u00c4ven p\u00e5 fem \u00e5rs sikt bed\u00f6mer Arbetsf\u00f6rmedlingen att m\u00f6jligheterna till arbete som anl\u00e4ggningsarbetare kommer vara stora.",
//   "stycke3": ""
