import {Observable, of, throwError} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {environment} from "../../environments/environment";
import {Injectable} from '@angular/core';
import {Searchparameter} from "../model/searchparameter.model";

@Injectable({
  providedIn: 'root'
})
export class SearchparametersService {

  constructor(private httpClient: HttpClient) {
  }

  private backendUrl = environment.settings.educationApi;

  public educationTypes = new Array<Searchparameter>();
  public educationForms = new Array<Searchparameter>();
  public municipalities = new Array<Searchparameter>();
  public regions = new Array<Searchparameter>();

  public getMunicipalities(): Observable<Searchparameter[]> {
    if (this.municipalities.length > 0) {
      return of(this.municipalities);
    }

    const url = `${this.backendUrl}/v1/searchparameters/municipalities`;
    return this.httpClient.get(url).pipe(catchError(this.handleError))
      .pipe(map(data => this.municipalities=SearchparametersService.handleSearchparametersResponse(data)));

  }

  public getRegions(): Observable<Searchparameter[]> {
    if (this.regions.length > 0) {
      return of(this.regions);
    }

    const url = `${this.backendUrl}/v1/searchparameters/regions`;
    return this.httpClient.get(url).pipe(catchError(this.handleError))
      .pipe(map(data => this.regions=SearchparametersService.handleSearchparametersResponse(data)));

  }

  public getEducationTypes(): Observable<Searchparameter[]> {
    if (this.educationTypes.length > 0) {
      return of(this.educationTypes);
    }

    const url = `${this.backendUrl}/v1/searchparameters/education_types`;
    return this.httpClient.get(url).pipe(catchError(this.handleError))
      .pipe(map(data =>
        this.educationTypes = SearchparametersService.handleSearchparametersResponse(data)
      ));

  }

  public getEducationForms(): Observable<Searchparameter[]> {
    if (this.educationForms.length > 0) {
      return of(this.educationForms);
    }

    const url = `${this.backendUrl}/v1/searchparameters/education_forms`;
    return this.httpClient.get(url).pipe(catchError(this.handleError))
      .pipe(map(data =>
        this.educationForms = SearchparametersService.handleSearchparametersResponse(data)
      ));

  }


  private static handleSearchparametersResponse(data: any) {
    const searchparametersResponse = new Array<Searchparameter>();
    for (let dataitem of data) {
      // console.log(dataitem)
      searchparametersResponse.push(dataitem)
    }

    return searchparametersResponse
  }


  handleError = (error: HttpErrorResponse) => {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

}
