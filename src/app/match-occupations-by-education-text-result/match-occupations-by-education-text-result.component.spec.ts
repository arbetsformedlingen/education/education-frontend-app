import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchOccupationsByEducationTextResultComponent } from './match-occupations-by-education-text-result.component';

describe('MatchOccupationsByEducationTextResultComponent', () => {
  let component: MatchOccupationsByEducationTextResultComponent;
  let fixture: ComponentFixture<MatchOccupationsByEducationTextResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchOccupationsByEducationTextResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchOccupationsByEducationTextResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
