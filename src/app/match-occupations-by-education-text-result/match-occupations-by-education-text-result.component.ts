import { Component, OnInit } from '@angular/core';
import {MatchedOccupations, RelatedOccupation} from "../model/matched-occupations.model";
import {MatchOccupationsByEducationTextService} from "../services/match-occupations-by-education-text.service";
import {EnrichedOccupationsService} from "../services/enriched-occupations.service";
import {EnrichedOccupation} from "../model/enriched-occupation";
import {AppComponent} from "../app.component";

@Component({
  selector: 'app-match-occupations-by-education-text-result',
  templateUrl: './match-occupations-by-education-text-result.component.html',
  styleUrls: ['./match-occupations-by-education-text-result.component.scss']
})
export class MatchOccupationsByEducationTextResultComponent implements OnInit {

  constructor(
    public matchOccupationsByEducationtextService: MatchOccupationsByEducationTextService,
    public enrichedOccupationsService: EnrichedOccupationsService,
    public appComponent: AppComponent
  ) { }

  ngOnInit(): void {
  }
  getRelatedOccupations(): RelatedOccupation[] | undefined {
    return this._getMatchedOccupations().relatedoccupations;
  }


  enrichedOccupations = new Map<string, EnrichedOccupation>();

  openedExpansion(occupationLabel: string, conceptTaxonomyId: string) {
    console.log('Opened expansion for occupation: ' + occupationLabel + ', taxonomyId: ' + conceptTaxonomyId)

    this.enrichedOccupationsService.getEnrichedOccupation(conceptTaxonomyId, true).subscribe((enrichedOccRes: EnrichedOccupation) => {
      {
        console.log('Got result for enriched occupation: ' + occupationLabel + ', taxonomyId: ' + conceptTaxonomyId)
        console.log(enrichedOccRes)
        this.enrichedOccupations.set(conceptTaxonomyId, enrichedOccRes);
      }
    });
  }

  public getEnrichedOccupation(conceptTaxonomyId: string): EnrichedOccupation{
    return this.enrichedOccupations.get(conceptTaxonomyId)!;
  }

  matchedOccupations = new MatchedOccupations()

  _getMatchedOccupations(): MatchedOccupations {
    this.matchOccupationsByEducationtextService.matchedOccupationsResult.subscribe((matchedRes: MatchedOccupations) => {
      {
        this.matchedOccupations = matchedRes
      }
    });
    return this.matchedOccupations
  }
}
